/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package test.yc.startup;

import com.alibaba.dubbo.container.Main;

/**
 * 描述：
 * 
 * <pre>HISTORY :
 * Created by 166046 at 2017年4月24日.
 * </pre>
 *
 * @author 
 */
public class DubboTestMain {

    public static void main(String[] args) {
        System.setProperty("system.no", "20");
        System.setProperty("system.version", "20");
        System.setProperty("system.name", "SGS-DUBBO-TEST");
        Main.main(args);
    }

}
