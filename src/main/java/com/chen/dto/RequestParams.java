package com.chen.dto;

import java.io.Serializable;

public class RequestParams implements Serializable {

    /**
     * 服务提供者的ip、端口号
     * eg: dubbo://localhost:20881
     */
    private String url;

    /**
     * RPC接口全类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 对应方法的请求参数
     */
    private String params;

    /**
     * 参数的全类名
     */
    private String paramsClassName;

    /**
     * 版本号（dubbo.version）
     */
//    private String version;

    /**
     * 分组（dubbo.group）
     */
    private String group;

    public RequestParams() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getParamsClassName() {
        return paramsClassName;
    }

    public void setParamsClassName(String paramsClassName) {
        this.paramsClassName = paramsClassName;
    }

//    public String getVersion() {
//        return version;
//    }

//    public void setVersion(String version) {
//        this.version = version;
//    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
