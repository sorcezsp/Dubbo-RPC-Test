package com.chen;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class TypeRef<T> { // NOSONAR
	private Type jt = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	/**
	 * 包内访问
	 * 
	 * @return
	 */
	Type getType() {
		return jt;
	}

}
