# Dubbo RPC接口测试工具 #
动态地调用RPC接口，方便对Dubbo RPC接口进行直接请求和测试。

## 接口实体 ##
Object restForRpc(RequestParams requestParams);

## 请求URL ##
http://10.118.60.177:8931/express/test/restForRpc

## 请求方式 ##
POST

## 请求参数 ##

| 参数名          | 必选 | 类型   |说明   |
| :-------------- | :----| :----- |:---------|
| url            |  Y  | String |dubbo服务的IP及端口号|
| className      | Y   | String |需要调用的RPC接口全类名|
| methodName     | Y   | String |需要调用的RPC接口的方法名|
| params         | Y   | String |需要调用的RPC接口方法的请求参数|
| paramsClassName| Y   | String |请求参数params的全类名|
| group          | N   | String |<dubbo:service />中设置了group时必传，没有设置则不传|
| version        | N   | String |<dubbo:service />中设置了version时必传，没有设置则不传|


## 说明 ##
只能调用注册到与该工具同一个zookeeper下的dubbo服务的RPC接口。